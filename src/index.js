import React, { Component } from "react";
import ReactDOM from "react-dom";
import "./index.css";
//import App from './App';
//import * as serviceWorker from './serviceWorker';

class Perros extends Component {
  state = {
    data: [],
  };

  componentDidMount() {
    console.log("Componente montado");
    //cargar datos de la api en el atributo de datos
    fetch(`https://dog.ceo/api/breeds/image/random`)
      .then(data => data.json())
      .then(data => this.setState(
        { data }
     
        ))
        
  }

  render() {
    return (
      <div>
        <h1>Perros </h1>
        {console.log(this.state.data)}
    <p>RAZA:{this.state.data.message}</p>
    <p>Mensaje:{this.state.data.status}</p>
      
      </div>
    );
  }
}

ReactDOM.render(
<Perros />, 
document.getElementById('root')
);
